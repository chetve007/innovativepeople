package task_2;

public class SortRandomIntApp {
    public static void main(String[] args) {

        SortRandomInt sortRandomInt = new SortRandomInt(15, "file.txt");

        sortRandomInt.createFileWithNumbers();
        sortRandomInt.sortAsc();
        sortRandomInt.sortDesc();

    }
}
