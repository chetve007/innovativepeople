package task_2;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;

public class SortRandomInt {

    private int count;
    private String fileName;
    private File file;

    public SortRandomInt() {
        count = 10;
        fileName = "Sample.txt";
        file = new File(System.getProperty("user.dir") + "/src/task_2/" + fileName);
        System.err.println("The default value is 10 and file name is 'Sample.txt'");
    }

    public SortRandomInt(int count, String fileName) {
        if (count <= 0)
            throw new RuntimeException("Number of numbers can not be negative or less than 1");
        if (fileName.equals(""))
            throw new RuntimeException("Uncorrected file");
        this.count = count;
        this.fileName = fileName;
        file = new File(System.getProperty("user.dir") + "/src/task_2/" + fileName);

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        if (count <= 0)
            throw new RuntimeException("Number of numbers can not be negative or less than 1");
        this.count = count;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        if (fileName.equals(""))
            throw new RuntimeException("Uncorrected file");
        this.fileName = fileName;
    }

    public void createFileWithNumbers() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (int i = 0; i < count; i++) {
                int random = (int) (Math.random() * 21);
                if (i < count - 1)
                    writer.write(String.valueOf(random) + ",");
                else
                    writer.write(String.valueOf(random));
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sortAsc() {
        Integer[] intNumbers = arrayForSort();
            Arrays.sort(intNumbers);
            System.out.printf("Sort array of integers in ascending order: %s \n", Arrays.toString(intNumbers));
    }

    public void sortDesc() {
            Integer[] intNumbers = arrayForSort();
            Arrays.sort(intNumbers, Collections.reverseOrder());
            System.out.printf("Sort array of integers in descending order: %s \n", Arrays.toString(intNumbers));
    }

    private Integer[] arrayForSort() {
        Integer[] intNumbers = new Integer[count];
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String[] numbers = reader.readLine().split(",");
            for (int i = 0; i < count; i++)
                intNumbers[i] = Integer.parseInt(numbers[i]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return intNumbers;
    }
}