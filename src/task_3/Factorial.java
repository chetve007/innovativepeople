package task_3;

public class Factorial {

    private static int num = 1;

    public static void main(String[] args) {

        Factorial factorial = new Factorial();

        System.out.println(factorial.factorialLoop(num));
        System.out.println(factorial.factorialRecursion(num));

    }

    public long factorialLoop(int num) {
        if (num < 1)
            throw new RuntimeException("Number must be greater than 0");
        long res = 1;
        for (int i = 1; i <= num; i++) {
            res *= i;
        }
        return res;
    }

    public long factorialRecursion(int num) {
        if (num < 1)
            throw new RuntimeException("Number must be greater than 0");
        if (num == 1)
            return 1;
        return factorialRecursion(num-1) * num;
    }
}